//
// ALPHA-g TPC
//
// Simulation unpacking functions
//
// Class functions for TPCSimEvent.h
//

#undef NDEBUG // this program requires working assert()

#include "TPCSimEvent.h"

#include <stdio.h>
//#include <string.h>
#include <assert.h> // assert()


TPCSimEvent::TPCSimEvent() // ctor
{
}

TPCSimEvent::~TPCSimEvent() // dtor
{
}

void TPCSimEvent::Print() const
{

}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
