//
// AgFlow.h
//
// manalyzer flow objects for ALPHA-g events
// K.Olchanski
//

#ifndef AgFlow_H
#define AgFlow_H

#include "AgEvent.h"
#include "unpack_cb.h"
#include "manalyzer.h"

class AgEventFlow: public TAFlowEvent
{
 public:
   AgEvent *fEvent;
   
 public:
 AgEventFlow(TAFlowEvent* flow, AgEvent* e) // ctor
    : TAFlowEvent(flow)
   {
      fEvent = e;
   }
   
   ~AgEventFlow() // dtor
      {
         if (fEvent) {
            delete fEvent;
            fEvent = NULL;
         }
      }
};

struct AgAwHit
{
   int adc_module; // ADC module, 1..20
   int adc_chan; // ADC channel, 0..15 and 16..47.
   int wire; // anode wire, 0..255 bottom, 256..511 top
   double time; // hit time, ns
   double amp;  // hit amplitude
};

class AgAwHitsFlow: public TAFlowEvent
{
 public:
   std::vector<AgAwHit> fAwHits;
   
 public:
 AgAwHitsFlow(TAFlowEvent* flow) // ctor
    : TAFlowEvent(flow)
   {
   }
};

struct AgBscAdcHit
{
   int adc_module; // ADC module, 1..20
   int adc_chan; // ADC channel, 0..15 and 16..47.
   int bar; // bar number, 0..63 bottom, 64..127 top
   double time; // hit time, ns
   double amp;  // hit amplitude
};

class AgBscAdcHitsFlow: public TAFlowEvent
{
 public:
   std::vector<AgBscAdcHit> fBscAdcHits;
   
 public:
 AgBscAdcHitsFlow(TAFlowEvent* flow) // ctor
    : TAFlowEvent(flow)
   {
   }
};

struct AgPadHit
{
   int imodule; // pwbNN
   int seqsca; // sca*80+ri
   int tpc_col; // pad column
   int tpc_row; // pad row
   double time_ns; // hit time in ns
   double amp;  // hit amplitude
};

class AgPadHitsFlow: public TAFlowEvent
{
 public:
   std::vector<AgPadHit> fPadHits;
   
 public:
 AgPadHitsFlow(TAFlowEvent* flow) // ctor
    : TAFlowEvent(flow)
   {
   }
};

class AgTrigUdpFlow: public TAFlowEvent
{
 public:
   std::vector<uint32_t> fData;
   
 public:
 AgTrigUdpFlow(TAFlowEvent* flow) // ctor
    : TAFlowEvent(flow)
   {
   }
};

class CbHitsFlow: public TAFlowEvent
{
public:
   std::string fCbBankName;
   int fCbIndex = 0; // 0=cbtrg, 1=cb01, etc
   int fNumInputs = 0;
   CbHits fHits;
   
public:
   CbHitsFlow(TAFlowEvent* flow) // ctor
      : TAFlowEvent(flow)
   {
   }
};

class CbScalersFlow: public TAFlowEvent
{
public:
   std::string fCbBankName;
   int fCbIndex = 0; // 0=cbtrg, 1=cb01, etc
   int fNumInputs = 0;
   CbLatchedScalers fScalers;
   
public:
   CbScalersFlow(TAFlowEvent* flow) // ctor
      : TAFlowEvent(flow)
   {
   }
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */


